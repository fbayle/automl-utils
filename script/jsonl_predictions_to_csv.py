#!/usr/bin/env python3

import pandas as pd
import json
import os
import subprocess
import glob
import re


def main(args):
    df = pd.DataFrame(columns=['id', 'solar', 'no_solar'])
    for jsonl_file in args.jsonl_in:
        print('Processing {f}'.format(f=jsonl_file))
        fname, _ = os.path.splitext(jsonl_file)
        fname_json = '{fname}.json'.format(fname=fname)
        fname_csv = '{fname}.csv'.format(fname=fname)
        cmd = "cat {f} | sed -e ':a' -e 'N' -e '$!ba' -e 's/\\n/,/g'  | sed 's/n/,/' | sed 's/^/[/'| sed 's/$/]/' > {fname_json}".format(
            f=jsonl_file, fname_json=fname_json)
        #cmd = re.escape(cmd)
        subprocess.call(cmd, shell=True)
        with open(fname_json) as json_file:
            data = json.load(json_file)

        ids = []
        solar_scores = []
        no_solar_scores = []
        for i in range(len(data)):
            f = data[i]
            ids.append(f['ID'].split("/")[-1])
            scores = f['annotations']
            solar_scores.append([s['classification']['score']
                                 for s in scores if s['display_name'] == 'solar'][0])
            no_solar_scores.append([s['classification']['score']
                                    for s in scores if s['display_name'] == 'no-solar'][0])

        df = pd.concat([df, pd.DataFrame({'id': ids, 'solar': solar_scores,
                                          'no_solar': no_solar_scores})], axis=0)

    print("Saving csv file ...")
    df.to_csv('predictions.csv', index=False)
    print('Done.')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Parse AUTOML Vision jsonl-results to csv ",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--jsonl_in',
        nargs='*',
        help='lists of paths of jsonl file')

    args = parser.parse_args()

    main(args)
