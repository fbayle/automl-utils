import fiona
import ee
import os

ee.Initialize()


def maskclouds(image):
    band_qa = image.select('QA60')
    cloud_mask = ee.Number(2).pow(10).int()
    cirrus_mask = ee.Number(2).pow(11).int()
    mask = band_qa.bitwiseAnd(cloud_mask).eq(0) and(
        band_qa.bitwiseAnd(cirrus_mask).eq(0))
    return image.updateMask(mask).divide(10000)


def obtain_image_sentinel(collection, time_range, area):
    """ Selection of median, cloud-free image from a collection of images in the Sentinel 2 dataset
    See also: https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2

    Parameters:
        collection (): name of the collection
        time_range (['YYYY-MT-DY','YYYY-MT-DY']): must be inside the available data
        area (ee.geometry.Geometry): area of interest

    Returns:
        sentinel_median (ee.image.Image)
     """
# First, method to remove cloud from the image
    sentinel_filtered = (ee.ImageCollection(collection).
                         filterBounds(area).
                         filterDate(time_range[0], time_range[1]).
                         filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20)).
                         map(maskclouds))
    sentinel_median = sentinel_filtered.median()
    return sentinel_median


files = ['data_gee/ln_buffer.shp',
         'data_gee/osm_poly_buffer.shp',
         'data_gee/osm_points_buffer.shp']
]

collection = 'COPERNICUS/S2'
for dataset in files:
    print('Processing file {f}.'.format(f=dataset))
    dataset_name, _ = os.path.splitext(os.path.basename(dataset))
    farms = fiona.open(dataset)
    time_range_farms = ['2019-06-01', '2020-02-15']
    for farm in farms:
        farm_coords = farm['geometry']['coordinates']
        fid = farm['properties']['dmx_id']
        patch_id = 'patch_{f}'.format(f=fid)
        area_farm = ee.Geometry.Polygon(farm_coords)
        img = obtain_image_sentinel(collection, time_range_farms, area_farm)
        out_image = ee.Image(img).select(['B4', 'B3', 'B2'])
        task = ee.batch.Export.image.toCloudStorage(
            image = out_image,
            bucket = 'dym-lnd-solar-panels-temp',
            fileNamePrefix = 'panels-osm/chequeaton/{d}/{f}'.format(
                d=dataset_name, f=fid),
            maxPixels = 8030040147504,
            scale = 10,
            region = area_farm)
        task.start()
        print(task.status())
